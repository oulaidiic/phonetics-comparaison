# -*- coding: utf-8 -*-
#author : Oulaidi M'hammed
import phonetics  
import numpy as np
# def levenshtein(s1, s2):
#     if len(s1) < len(s2):
#         return levenshtein(s2, s1)

#     # len(s1) >= len(s2)
#     if len(s2) == 0:
#         return len(s1)

#     previous_row = range(len(s2) + 1)
#     for i, c1 in enumerate(s1):
#         current_row = [i + 1]
#         for j, c2 in enumerate(s2):
#             insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
#             deletions = current_row[j] + 1       # than s2
#             substitutions = previous_row[j] + (c1 != c2)
#             current_row.append(min(insertions, deletions, substitutions))
#         previous_row = current_row
    
#     return previous_row[-1]
# -- initialize phonetics object


def levenshtein(seq1, seq2):  
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros ((size_x, size_y))
    for x in range(size_x):
        matrix [x, 0] = x
    for y in range(size_y):
        matrix [0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x-1] == seq2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )
    #print (matrix)
    return (matrix[size_x - 1, size_y - 1])

word1 = "Mhammed".strip().lower()
word2 = "Mohammed".strip().lower()
print ("Comparing %s with %s" % (word1, word2))
# -- phonetic code
# -- weight
weight = {
    "soundex": 0.3,
    "metaphone": 0.1,
    "dmetaphone": 0.1,
    "nysiis": 0.5
}
# -- algorithms
algorithms = ["soundex", "caverphone", "dmetaphone", "nysiis"]
# -- total
total = 0.0
soundex = 1-levenshtein(phonetics.soundex(word1),phonetics.soundex(word2))/max(len(phonetics.soundex(word1)),len(phonetics.soundex(word2)))
print("soundex : " ,soundex*100)
metaphone = 1-levenshtein(phonetics.metaphone(word1),phonetics.metaphone(word2))/max(len(phonetics.metaphone(word1)),len(phonetics.metaphone(word2)))
print("metaphone : ", metaphone*100)
dmetaphone =1-levenshtein(phonetics.dmetaphone(word1),phonetics.dmetaphone(word2))/max(len(phonetics.dmetaphone(word1)),len(phonetics.dmetaphone(word2)) )
nysiis = 1-levenshtein(phonetics.nysiis(word1),phonetics.nysiis(word2))/max(len(phonetics.nysiis(word1)),len(phonetics.nysiis(word2)))
print("double metaphone : " ,dmetaphone*100)
print("nysiis : ",nysiis*100)
total = (100*soundex*weight["soundex"])+(100*metaphone*weight["metaphone"])+(100*dmetaphone*weight["dmetaphone"])+(100*nysiis*weight["nysiis"])
print ("total: %0.2f" % total) 